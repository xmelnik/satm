import numpy as np

from typing import List, Tuple

Image = np.ndarray
Point = Tuple[int, int]
IdxIj = Tuple[int, int]
Contour = List[Point]

import matplotlib.pyplot as plt

import skfmm
from skimage.feature import peak_local_max
import math
from scipy.spatial import distance
from skimage import measure
from scipy.optimize import linear_sum_assignment
from scipy.interpolate import UnivariateSpline
from scipy import ndimage
import itertools

import operator
import collections

from typing import List, Tuple
from scipy.signal import find_peaks, peak_prominences
from visualization import show_array_of_images, ishow, show_cont_ges, visualize_contour


# l_weight = 3 # if equals to 1, then all three coordinates are equal
# 2 coord, max, no optional
def sat_ISBI(debug_mode, input_annotations_b, d_max, d4maxima):
    ge_settings = {"max": True, "min": False, "curv": False, "l": 1}
    l_weight = ge_settings["l"]
    ##############################################
    # topology check
    input_annotations = []

    for ano in input_annotations_b:
        connectedComp1 = measure.label(ano, return_num=True, connectivity=1)[0]
        sq = 0
        label_n = 0
        for i in range(1, np.max(connectedComp1) + 1):
            if np.sum(connectedComp1 == i) > sq:
                sq = np.sum(connectedComp1 == i)
                label_n = i
        one_cc_img = np.zeros(ano.shape)
        one_cc_img[connectedComp1 == label_n] = 1
        input_annotations.append(one_cc_img)

    #############################################
    all_contours = [get_contour(a) for a in input_annotations]

    #########################################################################
    # all contours have to starts approximately at the same point
    all_contours = rotate_contours_better(all_contours, debug_mode)

    # show_array_of_images(np.array(input_annotations))
    size_img = input_annotations[0].shape

    all_geodesic_ends = [get_geodesic_ends_3_coords_cont(cont, d4maxima, a, idx, debug_mode, ge_settings) for
                         idx, cont, a in
                         zip(range(len(input_annotations)), all_contours, input_annotations)]

    cont_img = []
    for cont in all_contours:
        ci = visualize_contour_ascending(input_annotations[0], cont)
        cont_img.append(ci)
    ########################################################################
    # creating auxiliary structures
    annotation_combinations = []
    N_annotations = len(all_geodesic_ends)
    for pair in itertools.combinations(range(N_annotations), 2):
        annotation_combinations.append(pair)

    ###############
    #  matching  #
    ##############

    graph_of_ij = GraphIj()

    for pair_comb in annotation_combinations:
        annotation1 = pair_comb[0]
        annotation2 = pair_comb[1]
        edges_from_matching = get_matches(all_geodesic_ends[annotation1], all_geodesic_ends[annotation2], d_max, l_weight)
        for e in edges_from_matching:
            graph_of_ij.add_edge((annotation1, e[0]), (annotation2, e[1]))  # desired input

    connected_components_ij = graph_of_ij.connected_components()

    cc_ij_gt1 = [cc for cc in connected_components_ij if len(cc) > 1]

    if debug_mode:
        print("Following are connected components [ij]=", len(cc_ij_gt1), 'components')
        for cc in cc_ij_gt1:
            print(cc, '=')
            for ij in cc:
                i1, j1 = ij
                print(ij, end='')
                print(all_geodesic_ends[i1][j1], end='')
            print()

    ############################
    #   iteration within CCs   #
    ############################

    final_contour = []

    if len(cc_ij_gt1) == 0:
        print('NO CC!!!, returning 1st')
        return input_annotations[0]

    cc_ij_gt1 = more_simple_sorting_avg_3coord(all_contours, cc_ij_gt1, all_geodesic_ends)

    idx_cc = 0
    while idx_cc < (len(cc_ij_gt1) - 1):
        cc1 = cc_ij_gt1[idx_cc]
        cc2 = cc_ij_gt1[idx_cc + 1]
        #################################################################
        # if there is a GE between the CC -> we exclude the contour from the CC (if its consist of more than 1 point ofc)
        # it is handled inside get_average_piece function
        #################################################################
        if debug_mode:
            print('cc1=', cc1)
            print('cc2=', cc2)
            show_cont_ges(cc1, all_contours, all_geodesic_ends, input_annotations[0].shape)
            show_cont_ges(cc2, all_contours, all_geodesic_ends, input_annotations[0].shape)

        if (idx_cc == len(cc_ij_gt1) - 2):  # last piece
            if debug_mode:
                print('___LAST PIECE____')
            avg_cont = get_average_piece_3coord(cc1, cc2, all_contours, True, all_geodesic_ends, size_img, d_max,
                                                debug_mode)  # averaging function
        else:
            avg_cont = get_average_piece_3coord(cc1, cc2, all_contours, False, all_geodesic_ends, size_img, d_max,
                                                debug_mode)  # averaging function

        #######################################################################################################
        # here we need to check the breaks between the last point of contour and the first point of new piece #
        #######################################################################################################
        if (len(final_contour) > 0 and len(avg_cont) > 0):
            last_point = final_contour[-1]
            round_p = avg_cont[0]
            if max(abs(last_point[0] - round_p[0]), abs(last_point[1] - round_p[1])) > 1:
                # we connect points if something wend wrong
                intermediate_points = get_intermediate(last_point, round_p)
                avg_cont = intermediate_points + avg_cont
        #######################################################################################################
        final_contour = final_contour + avg_cont
        idx_cc += 1
    #######################################################
    # check if the first and the last point are connected #
    #######################################################
    if len(final_contour) > 0:
        last_point = final_contour[-1]
        round_p = final_contour[0]
        if max(abs(last_point[0] - round_p[0]), abs(last_point[1] - round_p[1])) > 1:
            # we connect points if something wend wrong
            intermediate_points = get_intermediate(last_point, round_p)
            final_contour = final_contour + intermediate_points
            ###########################################################
    w, h = input_annotations[0].shape
    saf = fill_and_save_contour(final_contour, size_img[0], size_img[1])
    return saf


def more_simple_sorting_avg_3coord(all_contours, list_of_cc, all_geodesic_ends):
    sorted_of_cc = []
    for cc in list_of_cc:
        new_cc = [((i, j), all_geodesic_ends[i][j][2]) for i, j in cc]
        sum4avg = []
        for c in new_cc:
            sum4avg.append(c[1])
        new_cc.insert(0, sum(sum4avg) / len(sum4avg))
        sorted_of_cc.append(new_cc)
    sorted_of_cc.sort(key=lambda x: (x[0]))
    sorted_of_cc = [el[1:] for el in sorted_of_cc]
    sorted_of_cc.append(sorted_of_cc[0])
    return sorted_of_cc


def get_mutual_points4new(connected_component1: List[IdxIj], connected_component2: List[IdxIj], last_one) \
        -> List[Tuple[IdxIj, IdxIj]]:
    # mut_p = [(p1[0], p2[0]) for p1 in connected_component1 for p2 in connected_component2 if p1[0][0] == p2[0][0]]
    mut_point = []
    for p1 in connected_component1:
        for p2 in connected_component2:
            if (p1[0][0] == p2[0][0]):
                if p1[1] < p2[1]:
                    mut_point.append((p1[0], p2[0]))
                else:
                    # print('CHANGE - last one?')
                    if last_one:
                        mut_point.append((p1[0], p2[0]))
                    else:
                        mut_point.append((p2[0], p1[0]))
    return mut_point


def get_average_piece_3coord(connected_component1: List[IdxIj], connected_component2: List[IdxIj],
                             list_of_all_contours: List[Contour], last_one: bool, all_geodesic_ends, size_img, d_lim,
                             debug_mode) -> Contour:
    if debug_mode:
        print('average cont started')
    average_contour = []
    flag_for_direction = last_one
    img_sample = np.zeros(size_img)

    mutual_points = get_mutual_points4new(connected_component1, connected_component2, last_one)
    # mutual points are returned as [a1, a2 - b1, b2]

    if len(mutual_points) == 0:
        print('NO MUTUAL POINTS!')
        return []

    flag_annotation = np.zeros(len(all_geodesic_ends))
    mutual_points_without_dub = []

    for i_points in range(0, len(mutual_points)):
        an_num = mutual_points[i_points][0][0]
        if (flag_annotation[an_num] == 0):
            flag_annotation[an_num] = 1
            mutual_points_without_dub.append(mutual_points[i_points])

    if debug_mode:
        print('mutual_points_without_dub:', mutual_points_without_dub)
    mutual_points = mutual_points_without_dub
    ##############################################################
    # exclude here the piece of contour that has a GE in between the CC
    #############################################################
    all_pieces_to_average = []
    sums_lenth_pieces = []
    skipped = []
    vis_raw = []
    for i_points in range(0, len(mutual_points)):
        # here we have to average
        i1, j1 = mutual_points[i_points][0]  # these are the two points in the same annotation from 2 different cc
        i2, j2 = mutual_points[i_points][1]
        p1 = all_geodesic_ends[i1][j1]
        p2 = all_geodesic_ends[i2][j2]
        cont_3rd_1 = all_geodesic_ends[i1][j1][2]
        cont_3rd_2 = all_geodesic_ends[i2][j2][2]

        if flag_for_direction:
            if cont_3rd_1 < cont_3rd_2:
                p1, p2 = p2, p1
        else:
            if cont_3rd_1 > cont_3rd_2:
                p1, p2 = p2, p1

        if debug_mode:
            print('GEs= from ', p1, ' to ', p2)
            print('a number ', i1, i2, 'should be equal')
        # we collect all the contour pieces
        do_we_add, piece_of_contour = get_piece_of_contour4averaging_3coord(p1, p2, list_of_all_contours[i1], i1,
                                                                            all_geodesic_ends, debug_mode)
        if debug_mode:
            print('do_we_add? ', do_we_add)
        piece_con = visualize_contour(img_sample, piece_of_contour)
        vis_raw.append(piece_con)
        #########################################################################
        #
        #########################################################################
        if do_we_add:
            all_pieces_to_average.append(piece_of_contour)
            sums_lenth_pieces.append(len(piece_of_contour))
        elif len(all_pieces_to_average) == 0 and i_points == (
                len(mutual_points) - 1):  # we have to add something at least
            if debug_mode:
                print('even tho', do_we_add, ' we had to add smth')
            all_pieces_to_average.append(piece_of_contour)
            sums_lenth_pieces.append(len(piece_of_contour))
        else:
            skipped.append(i_points)
    ##################################################################
    ### OPTIONAL: we will go trouth it again and will add a piece if the length is not too far from the added
    # for i_points in skipped:
    #     # here we have to average
    #     i1, j1 = mutual_points[i_points][0]  # these are the two points in the same annotation from 2 different cc
    #     i2, j2 = mutual_points[i_points][1]
    #     p1 = all_geodesic_ends[i1][j1]
    #     p2 = all_geodesic_ends[i2][j2]
    #     if flag_for_direction:
    #         if cont_3rd_1 < cont_3rd_2:
    #             p1, p2 = p2, p1
    #     else:
    #         if cont_3rd_1 > cont_3rd_2:
    #             p1, p2 = p2, p1
    #     # ok here we need to collect all the contour pieces
    #     do_we_add, piece_of_contour = get_piece_of_contour4averaging_3coord(p1, p2, list_of_all_contours[i1], i1,
    #                                                                         all_geodesic_ends, debug_mode)
    #     if do_we_add:
    #         all_pieces_to_average.append(piece_of_contour)
    #     else:
    #         if (abs(len(piece_of_contour) - sum(sums_lenth_pieces) / len(
    #                 sums_lenth_pieces)) < d_lim * 2):  # THIS IT a parametr that depends on the average size
    #             if debug_mode:
    #                 print('even tho', do_we_add, ' we WILL add is its not that different')
    #             all_pieces_to_average.append(piece_of_contour)
    ####################################################################
    if debug_mode:
        ishow(vis_raw)

    length_list = []
    im_list = []
    for i in range(0, len(all_pieces_to_average)):
        length_list.append(len(all_pieces_to_average[i]))
        im = visualize_contour(img_sample,
                               all_pieces_to_average[
                                   i])  # this is a BUG FIX , put here an image sample
        # (size of the image is needed)
        im_list.append(im)
    # this is for visualisation
    if debug_mode:
        show_array_of_images(im_list)  ###########VISUAL show_array_of_images

    length_array = np.array(length_list)
    number_of_points_for_average_contour = int(np.max(length_array, axis=0))
    discretizations_to_average = [get_desired_discretization_for_contour(piece, number_of_points_for_average_contour)
                                  for piece in all_pieces_to_average]

    for i in range(0, number_of_points_for_average_contour):
        p_x = 0
        p_y = 0
        numb = 0
        for a_i in range(0, len(all_pieces_to_average)):
            if not (math.isnan(discretizations_to_average[a_i][i][0]) or math.isnan(
                    discretizations_to_average[a_i][i][1])):
                p_x += discretizations_to_average[a_i][i][0]
                p_y += discretizations_to_average[a_i][i][1]
                numb += 1
        p_x = p_x / numb
        p_y = p_y / numb
        if math.isnan(p_x) or math.isnan(p_y):
            print('NaN T_T')
        else:
            round_p = [int(round(p_x, 0)), int(round(p_y, 0))]
            #########################################################################
            # here we check that the contour is continuous
            #########################################################################
            if (len(average_contour) > 0):
                last_point = average_contour[-1]
                if (max(abs(last_point[0] - round_p[0]), abs(last_point[1] - round_p[1])) > 1):
                    intermediate_points = get_intermediate(last_point, round_p)
                    average_contour = average_contour + intermediate_points
            #########################################################################
            average_contour.append([round_p[0], round_p[1]])

    # this is the averages piece of contour between the 2 CCs
    return average_contour


def get_desired_discretization_for_contour(contour: Contour, desired_number_of_points: int) -> Contour:
    if len(contour) == desired_number_of_points:
        return contour
    coordinates_x = [x for x, y in contour]
    coordinates_y = [y for x, y in contour]

    # the spline function needs to get ascending points, so u gotta sort them

    points = np.array([coordinates_x,
                       coordinates_y]).T

    contour_length = np.cumsum(np.sqrt(np.sum(np.diff(points, axis=0) ** 2, axis=1)))
    contour_length = np.insert(contour_length, 0, 0) / contour_length[-1]

    # Build a list of the spline function, one for each dimension:
    splines = [UnivariateSpline(contour_length, coords, k=1, s=.9) for coords in points.T]

    # Computed the spline for the asked distances:
    alpha = np.linspace(0, 1, desired_number_of_points)
    points_fitted = np.vstack(spl(alpha) for spl in splines).T
    return points_fitted


def get_piece_of_contour4averaging_3coord(starting_point: Point, ending_point: Point, contour: Contour, anotation_i,
                                          all_geodesic_ends, debug_mode) -> (bool, Contour):
    # so we construct a new list of points
    # it has to check if none of is a GE with a high dynamic range
    piece_of_contour = []
    index_start = -1
    index_end = -1
    do_we_add = True

    for i in range(0, len(contour)):
        # print('starting_point', starting_point, 'contour[i]', contour[i])
        if starting_point[0] == contour[i][0] and starting_point[1] == contour[i][1]:
            index_start = i
            break
    for i in range(0, len(contour)):
        if ending_point[0] == contour[i][0] and ending_point[1] == contour[i][1]:
            index_end = i
            break
    if index_start < index_end:
        for i in range(index_start, index_end + 1):
            piece_of_contour.append(contour[i]);
    else:
        for i in range(index_start, len(contour)):
            piece_of_contour.append(contour[i]);
        for i in range(0, index_end + 1):
            piece_of_contour.append(contour[i]);

    cont_start = piece_of_contour[0]
    cont_end = piece_of_contour[-1]
    for point_c in piece_of_contour[1:-1]:  # if there is a GE in between then its empty contour
        for ge in all_geodesic_ends[anotation_i]:
            if (point_c[0] == ge[0] and point_c[1] == ge[1]) and (
                    point_c[0] != cont_start[0] and point_c[1] != cont_start[1]) and (
                    point_c[0] != cont_end[0] and point_c[1] != cont_end[1]):
                if debug_mode:
                    print('EMPTY_DUE_TO_GE3')
                do_we_add = False
    return do_we_add, piece_of_contour


def fill_and_save_contour(contour: Contour, width: int, height: int) -> Image:
    result_of_fusion = np.zeros((width, height), 'uint8')

    for point in contour:
        if point[0] < 0 or point[0] >= width or point[1] < 0 or point[1] >= height:
            print('-_-')
        else:
            result_of_fusion[point[0], point[1]] = 1

    fill_contour = ndimage.binary_fill_holes(result_of_fusion)
    fill_contour = fill_contour.astype(int)

    fill_contour[fill_contour > 0] = 255

    image_to_save = np.uint8(fill_contour)
    return image_to_save


def get_intermediate(p1, p2):
    nb_points = int(round(math.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2), 0))
    x_spacing = (p2[0] - p1[0]) / (nb_points + 1)
    y_spacing = (p2[1] - p1[1]) / (nb_points + 1)

    return [[int(round(p1[0] + i * x_spacing, 0)), int(round(p1[1] + i * y_spacing, 0))]
            for i in range(1, nb_points + 1)]


def visualize_contour_ascending(image: Image, contour: Contour) -> Image:
    visualization_image = np.zeros(image.shape)
    val = 0.1
    for point in contour:
        visualization_image[point[0], point[1]] = val
        val += 0.1
    return visualization_image


def rotate_contours_better(list_of_contours: List[Contour], debug_mode):
    copy_contours = [cont.copy() for cont in list_of_contours]
    if debug_mode:
        print('rotate_contours_better')
    for cont in copy_contours:
        cont.sort(key=operator.itemgetter(0))
        cont.sort(key=operator.itemgetter(1))
        if debug_mode:
            print('old ', cont[0])
    
    for cont in copy_contours:
        cont.sort(key = lambda x: (x[0]+x[1]))
        if debug_mode:
            print('new ', cont[0])
    rotated_contours = []

    starting_points = []
    for cont_idx in range(0, len(copy_contours)):
        point = copy_contours[cont_idx][0]
        index = (list_of_contours[cont_idx]).index(point)

        dq_list = collections.deque(list_of_contours[cont_idx])
        dq_list.rotate(-index)
        dq_new_list = list(dq_list)
        rotated_contours.append(dq_new_list)

    return rotated_contours


def get_contour(image: Image) -> Contour:  # we get a sorted border of cell that goes around cell clockwise
    contours = measure.find_contours(image, 0.9)  # 0.9 = outer contour
    contour = []
    contour_point = contours[0][0]
    int_val = [int(round(contour_point[0], 0)), int(round(contour_point[1], 0))]
    contour.append(int_val)
    for i in range(0, len(contours[0])):
        contour_point = contours[0][i]
        int_val = [int(round(contour_point[0], 0)), int(round(contour_point[1], 0))]
        if contour[len(contour) - 1] != int_val:
            contour.append(int_val)
    return contour


def get_geodesic_ends_3_coords_cont(contour, d_max, image, name, debug_mode, ge_settings) -> List[Point]:
    geodesic_vals = []
    geodesic_contour_map = np.zeros(image.shape)
    for contour_point in contour:
        geodesic_val = get_max_geodesic_path_length(image, contour_point)
        geodesic_vals.append(geodesic_val)
        if geodesic_contour_map[contour_point[0], contour_point[1]] < geodesic_val:  # does not work with 8 connectivity
            geodesic_contour_map[contour_point[0], contour_point[1]] = geodesic_val

    if debug_mode:
        ishow([geodesic_contour_map])

    geodesic_vals_extended = geodesic_vals.copy()
    if debug_mode:
        print('cont=', len(geodesic_vals))
    for i in range(d_max * 2):
        if i < len(geodesic_vals):
            geodesic_vals_extended.append(geodesic_vals[i])
    if debug_mode:
        print('cont=', len(geodesic_vals_extended))

    geodesic_func = np.array(geodesic_vals_extended)
    avg_geo = np.average(geodesic_func)
    avg_geo_max = np.max(geodesic_func)
    peaks, _ = find_peaks(geodesic_func, height=0, distance=d_max)
    prominences_peaks = peak_prominences(geodesic_func, peaks)[0]
    if debug_mode:
        print('FOUND ', len(peaks), 'PEAKS')

    bottoms, _ = find_peaks(-geodesic_func, height=-avg_geo_max, distance=d_max)
    prominences_bottoms = peak_prominences(-geodesic_func, bottoms)[0]

    if debug_mode:
        figSize_h = 7  # 15
        figSize_w = 10  # 30
        fig = plt.figure(figsize=(figSize_w, figSize_h))
        plt.plot(geodesic_func)
        plt.plot(peaks, geodesic_func[peaks], "x")
        contour_heights = geodesic_func[peaks] - prominences_peaks
        plt.vlines(x=peaks, ymin=contour_heights, ymax=geodesic_func[peaks])
        plt.plot(bottoms, geodesic_func[bottoms], "o")

    prominance2print_max = []
    prominance2print_min = []

    geodesic_maxi_scipy = []
    geodesic_mini_scipy = []

    pro_peaks = 3
    if ge_settings['max']:
        pro_peaks = 3
    else:
        pro_peaks = 50

    pro_bottoms = 50
    if ge_settings['min']:
        pro_bottoms = 6
    else:
        pro_bottoms = 50

    for i, peak in enumerate(peaks):
        if peak < len(contour) and prominences_peaks[i] > pro_peaks:
            point = (contour[peak][0], contour[peak][1], round(peak / len(contour) * 100, 3))
            geodesic_maxi_scipy.append(point)
            prominance2print_max.append(prominences_peaks[i])
        elif prominences_peaks[i] > pro_peaks:
            point = (contour[peak - len(contour) + 1][0], contour[peak - len(contour) + 1][1],
                     round((peak - len(contour) + 1) / len(contour) * 100, 3))
            geodesic_maxi_scipy.append(point)
            prominance2print_max.append(prominences_peaks[i])
    for i, bottom in enumerate(bottoms):
        if bottom < len(contour) and prominences_bottoms[i] > pro_bottoms:
            point = (contour[bottom][0], contour[bottom][1], round(bottom / len(contour) * 100, 3))
            geodesic_mini_scipy.append(point)
            prominance2print_min.append(prominences_bottoms[i])
        elif prominences_bottoms[i] > pro_bottoms:
            point = (contour[bottom - len(contour) + 1][0], contour[bottom - len(contour) + 1][1],
                     round((bottom - len(contour) + 1) / len(contour) * 100, 3))
            geodesic_mini_scipy.append(point)
            prominance2print_min.append(prominences_bottoms[i])

    local_maxi2 = peak_local_max(geodesic_contour_map, footprint=np.ones(((d_max, d_max))))  # used to be 20x 20  div 2
    geodesic_ends = local_maxi2.tolist()
    ## min ##
    geodesic_contour_map2 = -1.0 * geodesic_contour_map
    geodesic_contour_map2[geodesic_contour_map2 == 0] = float('-inf')

    local_maxi2_2 = peak_local_max(geodesic_contour_map2,
                                   footprint=np.ones(((d_max, d_max))))  # used to be 20x 20  div 2
    geodesic_ends_2 = local_maxi2_2.tolist()

    if debug_mode:
        print('max= ', geodesic_maxi_scipy)
        print(prominance2print_max)
        print('min= ', geodesic_mini_scipy)
        print(prominance2print_min)

    # curv = high_curvature_points_3coord(contour, 60, 30)
    ######################################################################
    if debug_mode:
        figSize_h = 15  # 15
        figSize_w = 10  # 30
        fig = plt.figure(figsize=(figSize_w, figSize_h))
        plt.axis('off')
        fig.tight_layout()
        plt.imshow(geodesic_contour_map)  # ,cmap='gray')
        plt.scatter(contour[0][1], contour[0][0], s=400, c='white', marker='x')
        for point in geodesic_maxi_scipy:
            plt.scatter(point[1], point[0], s=400, c='red', marker='o')
        for point in geodesic_mini_scipy:
            plt.scatter(point[1], point[0], s=400, c='blue', marker='*')
        plt.show()
        print(geodesic_ends)
        print(geodesic_ends_2)

    # mystamp
    # return geodesic_maxi_scipy
    ### return geodesic_mini_scipy + geodesic_maxi_scipy
    ####return geodesic_mini_scipy + geodesic_maxi_scipy  # geodesic_mini_scipy#geodesic_maxi_scipy
    # return curv+geodesic_maxi_scipy
    # return geodesic_mini_scipy+geodesic_maxi_scipy+curv
    if ge_settings['min']:
        return geodesic_mini_scipy + geodesic_maxi_scipy
    else:
        return geodesic_maxi_scipy


def get_max_geodesic_path_length(image: Image, path_starting_point: Point) -> float:
    mask = np.zeros(image.shape)
    mask[image == 0] = 1
    phi = np.ones(image.shape)
    phi = np.ma.masked_array(phi, mask)
    phi[path_starting_point[0], path_starting_point[1]] = -1
    distance_map = skfmm.distance(phi)
    return np.max(distance_map)


# partly taken from the internet https://www.geeksforgeeks.org/connected-components-in-an-undirected-graph/
class GraphIj:
    # init function to declare class variables
    def __init__(self):
        self.vertexes = []
        self.adj = []

    def dfs_util(self, temp: List[int], v: int, visited: List[int]):
        # Mark the current vertex as visited
        visited[v] = True
        # Store the vertex to list
        temp.append(v)
        # Repeat for all vertices adjacent
        # to this vertex v
        for i in self.adj[v]:
            if not visited[i]:
                # Update the list
                temp = self.dfs_util(temp, i, visited)
        return temp

    # method to add an undirected edge
    def add_edge(self, v: IdxIj, w: IdxIj):
        found_v = False
        found_w = False
        idx_v = 0
        idx_w = 0
        for idx, el in enumerate(self.vertexes):
            if v == el:
                found_v = True
                idx_v = idx
            if w == el:
                found_w = True
                idx_w = idx
        if found_w and found_v:
            self.adj[idx_v].append(idx_w)
            self.adj[idx_w].append(idx_v)

        if not found_v:
            self.vertexes.append(v)
            self.adj.append([])
            idx_v = len(self.vertexes) - 1
        if not found_w:
            self.vertexes.append(w)
            self.adj.append([])
            idx_w = len(self.vertexes) - 1
        self.adj[idx_v].append(idx_w)
        self.adj[idx_w].append(idx_v)

    # Method to retrieve connected components
    # in an undirected graph
    def connected_components(self) -> List[List[IdxIj]]:
        visited = []
        cc = []
        cc_objects = []
        for i in range(0, len(self.vertexes)):
            visited.append(False)
        for v in range(0, len(self.vertexes)):
            if not visited[v]:
                temp = []
                cc.append(self.dfs_util(temp, v, visited))
                # cc_objects(self.vertexes[self.dfs_util(temp, v, visited)])
        for comp in cc:
            tmp_c = []
            for c in comp:
                tmp_c.append(self.vertexes[c])
            cc_objects.append(tmp_c)
        return cc_objects


def get_matches(l1: List[Point], l2: List[Point], distance_threshold: int, l_weight: float) -> List[IdxIj]:
    # l1_e = [[e[0], e[1], e[2] * (1 / math.sqrt(l_weight))] for e in l1]
    # l2_e = [[e[0], e[1], e[2] * (1 / math.sqrt(l_weight))] for e in l2]
    # l1 = l1_e
    # l2 = l2_e

    l1_e = [e[0:2] for e in l1]
    l2_e = [e[0:2] for e in l2]
    l1 = l1_e
    l2 = l2_e

    A = cost_matrix(l1, l2, distance_threshold)
    row_ind, col_ind = linear_sum_assignment(A)
    ij_matched = []
    for i in range(0, max(len(l1), len(l2))):
        match = True
        if row_ind[i] >= len(l1):
            match = False
        if col_ind[i] >= len(l2):
            match = False
        if match:
            ij_matched.append((row_ind[i], col_ind[i]))  # j1 , j2
    return ij_matched


def cost_matrix(point_list1: List[Point], point_list2: List[Point], distance_threshold: int) -> np.ndarray:
    # coordinates [x,y]
    K = len(point_list1)
    L = len(point_list2)
    A = np.zeros(shape=(K + L, K + L))
    for i in range(0, K):
        for j in range(0, L):
            A[i, j] = distance.euclidean(point_list1[i], point_list2[j]) - distance_threshold
    return A
