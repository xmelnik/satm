
import numpy as np

from typing import List, Tuple

Image = np.ndarray
Point = Tuple[int, int]
IdxIj = Tuple[int, int]
Contour = List[Point]


import fdasrsf as fs
import numpy as np
import matplotlib.pyplot as plt
import math
from skimage import measure
from SATMean import get_contour, fill_and_save_contour, get_desired_discretization_for_contour
from visualization import ishow

def apply_KM(array_img, islarge):
    ##############################################
    # topology check
    input_annotations = []

    for ano in array_img:
        connectedComp1 =measure.label(ano, return_num=True, connectivity=1)[0]
        sq = 0
        label_n = 0
        for i in range(1, np.max(connectedComp1 ) +1):
            if np.sum(connectedComp1 == i) > sq:
                sq = np.sum(connectedComp1 == i)
                label_n = i
        one_cc_img =np.zeros(ano.shape)
        one_cc_img[connectedComp1==label_n ] =1
        input_annotations.append(one_cc_img)
    array_img = input_annotations



    contours_i = []
    cont_imgs = []

    for k, img in enumerate(array_img):
        contours_i.append(get_contour(img))
        imgc = get_contour_img(img)
        cont_imgs.append(imgc)

    max_l = 0
    for i, cont in enumerate(contours_i):
        if len(cont ) >max_l:
            max_l = len(cont)
    print('max= ', max_l)

    if islarge:
        # num_points = (int)(max_l/2)
        if max_l < 100:
            num_points = max_l
        elif max_l >= 100 and max_l < 500:
            num_points = (int)(max_l/3)
        elif max_l >= 500 and max_l < 1000:
            num_points = (int)(max_l/4)
        elif max_l >= 1000 and max_l < 1500:
            num_points = (int)(max_l/6)
        elif max_l >= 1500 and max_l < 2000:
            num_points = (int)(max_l/8)
        else:
            num_points = (int)(max_l/10)
        num_points = max_l
    else:
        num_points = max_l *2
    contours_disc = [get_desired_discretization_for_contour(piece, num_points)
                     for piece in contours_i]

    K  = len(contours_disc)
    M = num_points
    n = 2
    beta = np.zeros((n ,M ,K))
    for i in range(0 ,K):
        beta[: ,: ,i] = np.transpose(np.array(contours_disc[i]))
        x = beta[0 ,: ,i]
        y = beta[1 ,: ,i]
        plt.plot(x, y)

    obj = fs.fdacurve(beta, mode='C' ,N=M, scale=True)
    obj.karcher_mean()

    x, y = obj.beta_mean

    image_mean = np.zeros(array_img[0].shape)
    w, h  = array_img[0].shape
    centroids = obj.cent
    c_x, c_y = centroids.mean(axis=1)
    final_cont = []
    for i ,j in zip(x ,y):
        if not (int( i +c_x ) <0 or int( i +c_x )>=w or int( j +c_y ) <0 or int( j +c_y )>=h):
            image_mean[int( i +c_x) ,int( j +c_y) ] =1
            final_cont.append(((int( i +c_x) ,int( j +c_y))))

    new_f =[]
    old_cont = []
    failll = False
    if islarge:
        new_length = len(final_cont)
        final_cont_lar = get_desired_discretization_for_contour(final_cont, max_l * 2)
        try:
            for point in final_cont_lar:
                int_point = ((int)(point[0]), (int)(point[1]))
                new_f.append(int_point)
        except Exception as e:
            failll = True
            print('EXEPTION: ', e)
            obj.plot()
            try:
                final_cont_lar = get_desired_discretization_for_contour(final_cont, new_length * 3)
                for point in final_cont_lar:
                    int_point = ((int)(point[0]), (int)(point[1]))
                    new_f.append(int_point)
            except Exception as e:
                failll = True
                print('EXEPTION 222: ', e)
        if not failll:
            final_cont = new_f
        else:  # it fails, then lets interpolate from point to point
            print('INTERMEDIATE')
            inter_cont = []
            inter_cont.append(final_cont[0])
            # print('len=', len(final_cont))
            for idx_po in range(1, len(final_cont)):
                cur = final_cont[idx_po]
                prev = final_cont[idx_po - 1]
                if (max(abs(prev[0] - cur[0]), abs(prev[1] - cur[1])) > 1):
                    intermediate_points = get_intermediate_km(prev, cur)
                    inter_cont += intermediate_points
                else:
                    inter_cont.append(prev)
                    inter_cont.append(cur)

            final_cont = inter_cont

    inter_cont = []
    inter_cont.append(final_cont[0])
    # print('len=', len(final_cont))
    for idx_po in range(1, len(final_cont)):
        cur = final_cont[idx_po]
        prev = final_cont[idx_po - 1]
        if (max(abs(prev[0] - cur[0]), abs(prev[1] - cur[1])) > 1):
            print('####NO closing_POINT_!!!!!!')
            # here, u have to connect the points
            intermediate_points = get_intermediate_km(prev, cur)
            inter_cont += intermediate_points
        else:
            inter_cont.append(prev)
            inter_cont.append(cur)

    final_cont = inter_cont
    # failll= True
    image_mean_filled = fill_and_save_contour(final_cont, w, h)
    if failll:
        ishow([array_img[0], image_mean, image_mean_filled])
    return image_mean_filled


def get_intermediate_km(p1, p2):
    nb_points = int(round(math.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2), 0))
    x_spacing = (p2[0] - p1[0]) / (nb_points + 1)
    y_spacing = (p2[1] - p1[1]) / (nb_points + 1)

    return [[int(round(p1[0] + i * x_spacing, 0)), int(round(p1[1] + i * y_spacing, 0))]
            for i in range(0, nb_points + 1)]


def visualize_contour_km(image, contour):
    visualization_image = np.zeros(image.shape)
    val = 1
    for point in contour:
        visualization_image[point[0], point[1]] = 1  # val
        val += 1
    return visualization_image


def get_contour_img(image):  # we get a sorted border of cell that goes around cell clockwise
    contours = measure.find_contours(image, 0.9)
    contour = []
    contour_point = contours[0][0]
    int_val = [int(round(contour_point[0], 0)), int(round(contour_point[1], 0))]
    contour.append(int_val)
    for i in range(0, len(contours[0])):
        contour_point = contours[0][i]
        int_val = [int(round(contour_point[0], 0)), int(round(contour_point[1], 0))]
        if contour[len(contour) - 1] != int_val:
            contour.append(int_val)
    return visualize_contour_km(image, contour)
