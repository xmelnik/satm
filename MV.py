
import numpy as np
from skimage import io
import skimage
import SimpleITK as sitk
from visualization import ishow

def apply_MV_PY_CTC(cells_cropped_border):
    # here we compute MV
    a_sum = np.zeros(cells_cropped_border[0].shape)
    for a in cells_cropped_border:
        a[a>0]=1
        a_sum = a_sum+a
    res = np.zeros(cells_cropped_border[0].shape)
    tresh = 2
    if len(cells_cropped_border)==1:
        tresh = 1
    elif len(cells_cropped_border)==2:
        tresh = 2
    res[a_sum>=tresh]=1 # this is MV for the cells
    mv_t = np.zeros(cells_cropped_border[0].shape)
    mv_t[res>0]=1
    return mv_t


def apply_MV_PY(cells_cropped_border):
    # here we compute MV
    total_number = len(cells_cropped_border)
    a_sum = np.zeros(cells_cropped_border[0].shape)
    for a in cells_cropped_border:
        a[a>0]=1
        a_sum = a_sum+a
    res = np.zeros(cells_cropped_border[0].shape)
    tresh = total_number//2
    res[a_sum>tresh]=1 # this is MV for the cells
    return res


def majorityVotingFromSITK(segmentations):
    labelForUndecidedPixels = 1
    reference_segmentation_majority_vote = sitk.LabelVoting(segmentations, labelForUndecidedPixels)
    manual_plus_majority_vote = list(segmentations)
    # Append the reference segmentation to the list of manual segmentations
    manual_plus_majority_vote.append(reference_segmentation_majority_vote)
    arr = sitk.GetArrayFromImage(reference_segmentation_majority_vote)
    return arr

def apply_MV_sitk(array_img):
    folder_global = '/home/alex/Desktop/NEW_experiment/ICIP_paper_figures/staple/'

    number_of_images = len(array_img)
    for i, cont in enumerate(array_img):
        cont[cont > 0] = 1
        cont = np.uint8(cont)
        skimage.io.imsave(fname=folder_global + str(i) + '_sitk.tif', arr=cont, check_contrast=False)

    testForSTAPLEimgs = [sitk.ReadImage((folder_global + str(name) + '_sitk.tif'), sitk.sitkUInt8) for name in
                         range(number_of_images)]

    MASK_MV = majorityVotingFromSITK(testForSTAPLEimgs)
    mask2save = MASK_MV
    mask2save[mask2save > 0] = 1
    return mask2save