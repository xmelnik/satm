from promfusion.FusionProbabilityMaps import FusionProbabilityMaps
from config import config
import numpy as np


def get_array(list_img):
    w,h = list_img[0].shape
    res = np.zeros((len(list_img), w,h))
    for i, img in enumerate(list_img):
        res[i][img>0]=1.0
    return res


def apply_RBF(array_img, tre):
    rates = get_array(array_img)
    img = array_img[0]
    raters_masks = rates
    spacing = np.ones(2)

    # Find a consensus
    FusionMap = FusionProbabilityMaps(config)
    FusionMap.find_consensus(img, raters_masks, spacing)

    # In case we asked to crop the image in the config file, we need to get the cropped version of the image
    # and raters' masks
    img = FusionMap.img
    raters_masks = FusionMap.raters_masks
    consensus = FusionMap.consensus[0]
    res = np.zeros(img.shape)
    res[consensus > tre] = 1.0
    return res

