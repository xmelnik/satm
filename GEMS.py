from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import cv2 as cv
import skimage
import numpy as np
import matplotlib.pyplot as plt
from skimage.feature import peak_local_max
from skimage import measure
from skimage.segmentation import watershed
from visualization import ishow

def apply_GEMSp(array_imgs, rad_t, debug_mode):
    ##############################################
    # topology check
    input_annotations = []

    for ano in array_imgs:
        connectedComp1 = measure.label(ano, return_num=True, connectivity=1)[0]
        sq = 0
        label_n = 0
        for i in range(1, np.max(connectedComp1) + 1):
            if np.sum(connectedComp1 == i) > sq:
                sq = np.sum(connectedComp1 == i)
                label_n = i
        one_cc_img = np.zeros(ano.shape)
        one_cc_img[connectedComp1 == label_n] = 1
        input_annotations.append(one_cc_img)
    array_imgs = input_annotations
    ##########################################################

    contours_i = []
    for img in array_imgs:
        contours_i.append(get_contour_img_gems(img))

    number_of_images = len(array_imgs)

    folder_global = 'tmp/'
    for i, cont in enumerate(contours_i):
        cont[cont > 0] = 255
        cont = np.uint8(cont)
        skimage.io.imsave(fname=folder_global + str(i) + '_cv.tif', arr=cont, check_contrast=False)

    imgs = [cv.imread(folder_global + str(name) + '_cv.tif', 0) for name in range(number_of_images)]

    gems_result = GEMS(imgs, rad_t, debug_mode)

    mask2save = gems_result
    mask2save[mask2save > 0] = 255
    mask2save = np.uint8(mask2save)
    return mask2save


def GEMS(imgs, rad_t, debug_mode):
    phi = computeDtistanceTransformSum4ArrayOfImagesSKI(imgs, False);
    PHI_inv = invertEDT(phi, debug_mode);
    if debug_mode:
        plt.imshow(phi, cmap='gray')
    hull = convexHull4setOfContours(imgs, debug_mode);
    H = showConveHullPolygonFromArray(hull, imgs, debug_mode);
    H = H.astype('bool')
    H_inv = ~H
    local_maxi = peak_local_max(phi, indices=False, footprint=np.ones((rad_t, rad_t)))
    markers = np.zeros(imgs[0].shape)
    markers[local_maxi == True] = 1
    markers[H_inv == True] = 2
    # ishow([markers])
    labels = watershed(PHI_inv, markers)
    labels[labels == 2] = 0
    labels[labels == 1] = 255
    if debug_mode:
        plt.imshow(labels, cmap='gray')
    createANDsave1imageANDarrayOfContours(labels, imgs, debug_mode)
    return labels



def createANDsave1imageANDarrayOfContours(img, imgs, debug_mode):
    row, cols = imgs[0].shape
    img3C = np.zeros((row, cols, 3))
    img3C = img3C.astype(np.uint8)
    for i in range(0, row):
        for j in range(0, cols):
            vertex = False
            for k in range(0, len(imgs)):
                vertex = vertex or (imgs[k][i, j] == 0)
                if vertex:
                    break
            if (vertex):
                img3C[i, j] = [0, 0, 255]
            elif (img[i, j] > 0):
                img3C[i, j] = [0, 255, 0]
            else:
                img3C[i, j] = [0, 0, 0]
    if debug_mode:
        plt.imshow(img3C)
        # cv.imwrite('tmp/res_3c.tif', img3C)


def invertEDT(img, debug_mode):
    maxV = np.max(img)
    img = maxV - img
    if debug_mode:
        plt.imshow((img), cmap='gray')
    return img


# returs sum euclidean distance for array of images
# shows the euclidean distance for all images and for one
def computeDtistanceTransformSum4ArrayOfImagesSKI(imgs, debug_mode):
    shape = imgs[0].shape
    phi = np.zeros(shape)
    arrayOfDT = []
    for i in range(0, len(imgs)):
        dist = cv.distanceTransform(imgs[i], cv.DIST_L2, 3)
        # tmp=np.asarray(dist[:,:])
        arrayOfDT.append(dist)
        phi += dist
    arrayOfDT.append(phi)
    if (debug_mode):
        ishow(arrayOfDT)
    return phi


# returs sum euclidean distance for array of images
# shows the euclidean distance for all images and for one
def computeDtistanceTransformSum4ArrayOfImages(imgs, debug_mode):
    shape = imgs[0].shape
    phi = np.zeros(shape)
    arrayOfDT = []
    for i in range(0, len(imgs)):
        dist = ndi.distance_transform_edt(imgs[i])
        tmp = dist  # np.asarray(dist[:,:])
        arrayOfDT.append(tmp)
        phi += tmp
    arrayOfDT.append(phi)
    if debug_mode:
        ishow(arrayOfDT)
    return phi


def showConveHullPolygonFromArray(hull, imgs, debug_mode):
    listOfPoints = []
    row, col, z = hull.shape
    for i in range(0, row):
        val = hull[i, :, :]
        listOfPoints.append([val[0][0], val[0][1]])
    polygon = Polygon(np.asarray(listOfPoints))
    rowP, colP = imgs[0].shape
    convexHullPic = np.zeros(imgs[0].shape)
    for i in range(0, rowP):
        for j in range(0, colP):
            point = Point(i, j)
            if (polygon.contains(point)):
                convexHullPic[i, j] = 255
    if debug_mode:
        plt.imshow(convexHullPic, cmap='gray')
    return convexHullPic


# comute convex hull for array of contour images
# return array of points (convex hull contour)
def convexHull4setOfContours(imgs, debug_mode):
    idxVertexList = []
    unionOfCont = np.zeros(imgs[0].shape)
    row, cols = imgs[0].shape
    for i in range(0, row):
        for j in range(0, cols):
            vertex = False
            for k in range(0, len(imgs)):
                vertex = vertex or (imgs[k][i, j] == 0)
                if vertex:
                    break
            if (vertex):
                idxVertexList.append([i, j])
                unionOfCont[i, j] = 1
    hull = cv.convexHull(np.asarray(idxVertexList));
    if debug_mode:
        print('convex hull has been computed')
        fig = plt.figure(figsize=(15, 15))
        plt.imshow(unionOfCont)
    return hull


def visualize_contour_gems(image, contour):
    visualization_image = np.zeros(image.shape)
    visualization_image[:]=255
    val =1
    for point in contour:
        visualization_image[point[0], point[1]] = 0#val
        val += 1
    point0 = contour[0]
    return visualization_image


# TODO: is it correct ? last point and first point are weird
def get_contour_img_gems(image):  # we get a sorted border of cell that goes around cell clockwise
    contours = measure.find_contours(image,
                                     0.9)  # 0.9 find all contours in the image (0.9 = inner contour!), (0.1 = outer contour!)
    contour = []
    contour_point = contours[0][0]
    int_val = [int(round(contour_point[0], 0)), int(round(contour_point[1], 0))]
    contour.append(int_val)
    for i in range(0, len(contours[0])):
        contour_point = contours[0][i]
        int_val = [int(round(contour_point[0], 0)), int(round(contour_point[1], 0))]
        if contour[len(contour) - 1] != int_val:
            contour.append(int_val)
    return visualize_contour_gems(image, contour)
