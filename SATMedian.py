import numpy as np
from typing import List, Tuple

Image = np.ndarray
Point = Tuple[int, int]
IdxIj = Tuple[int, int]
Contour = List[Point]


from scipy.interpolate import interp1d
import numpy as np
import math
from skimage import measure
import itertools
from typing import List, Tuple


from visualization import show_array_of_images, ishow, show_cont_ges, visualize_contour
from SATMean import get_contour, GraphIj, rotate_contours_better, get_geodesic_ends_3_coords_cont, \
    visualize_contour_ascending, get_matches, more_simple_sorting_avg_3coord, fill_and_save_contour, \
    get_intermediate, get_mutual_points4new, get_piece_of_contour4averaging_3coord


# no optional
def sat_median(debug_mode, input_annotations_b, d_max, d4maxima):
    ge_settings = {"max": True, "min": False, "curv": False, "l": 3}
    l_weight = ge_settings["l"]
    ##############################################
    # topology check
    input_annotations = []

    for ano in input_annotations_b:
        connectedComp1 = measure.label(ano, return_num=True, connectivity=1)[0]
        sq = 0
        label_n = 0
        for i in range(1, np.max(connectedComp1) + 1):
            if np.sum(connectedComp1 == i) > sq:
                sq = np.sum(connectedComp1 == i)
                label_n = i
        one_cc_img = np.zeros(ano.shape)
        one_cc_img[connectedComp1 == label_n] = 1
        input_annotations.append(one_cc_img)


    #############################################
    all_contours = [get_contour(a) for a in input_annotations]

    #########################################################################
    # all contours have to starts approximately at the same point
    all_contours = rotate_contours_better(all_contours, debug_mode)

    size_img = input_annotations[0].shape

    all_geodesic_ends = [get_geodesic_ends_3_coords_cont(cont, d4maxima, a, idx, debug_mode, ge_settings) for idx, cont, a in
                         zip(range(len(input_annotations)), all_contours, input_annotations)]

    cont_img = []
    for cont in all_contours:
        ci = visualize_contour_ascending(input_annotations[0], cont)
        cont_img.append(ci)

    ########################################################################
    # creating auxiliary structures
    annotation_combinations = []
    N_annotations = len(all_geodesic_ends)
    for pair in itertools.combinations(range(N_annotations), 2):
        annotation_combinations.append(pair)

    ###############
    #  matching  #
    ##############

    graph_of_ij = GraphIj()

    for pair_comb in annotation_combinations:
        annotation1 = pair_comb[0]
        annotation2 = pair_comb[1]
        edges_from_matching = get_matches(all_geodesic_ends[annotation1], all_geodesic_ends[annotation2], d_max, l_weight)
        for e in edges_from_matching:
            graph_of_ij.add_edge((annotation1, e[0]), (annotation2, e[1]))  # desired input

    connected_components_ij = graph_of_ij.connected_components()

    cc_ij_gt1 = [cc for cc in connected_components_ij if len(cc) > 1]

    if debug_mode:
        print("Following are connected components [ij]=", len(cc_ij_gt1), 'components')
        for cc in cc_ij_gt1:
            print(cc, '=')
            for ij in cc:
                i1, j1 = ij
                print(ij, end='')
                print(all_geodesic_ends[i1][j1], end='')
            print()

    ############################
    #   iteration within CCs   #
    ############################

    final_contour = []
    if len(cc_ij_gt1) == 0:
        print('NO CC!!!, returning 1st')
        return input_annotations[0]
    cc_ij_gt1 = more_simple_sorting_avg_3coord(all_contours, cc_ij_gt1, all_geodesic_ends)

    idx_cc = 0
    while idx_cc < (len(cc_ij_gt1) - 1):

        cc1 = cc_ij_gt1[idx_cc]
        cc2 = cc_ij_gt1[idx_cc + 1]
        #################################################################
        # if there is a GE between the CC -> we exclude the contour from the CC (if it consists of more than 1 point ofc)
        # it is handled inside get_average_piece function
        #################################################################
        # MEDIAN averaging
        #################################################################
        if (True):  # will it work with new CCs ?
            if debug_mode:
                print('cc1=', cc1)
                print('cc2=', cc2)
            if debug_mode:
                show_cont_ges(cc1, all_contours, all_geodesic_ends, input_annotations[0].shape)
                show_cont_ges(cc2, all_contours, all_geodesic_ends, input_annotations[0].shape)

        if (idx_cc == len(cc_ij_gt1) - 2):  # last piece
            if debug_mode:
                print('___LAST PIECE____')
            avg_cont = get_average_piece_3coord_MEDIAN(cc1, cc2, all_contours, True, all_geodesic_ends, size_img, d_max,
                                                       debug_mode, input_annotations_b)  # averaging function
        else:
            avg_cont = get_average_piece_3coord_MEDIAN(cc1, cc2, all_contours, False, all_geodesic_ends, size_img,
                                                       d_max, debug_mode, input_annotations_b)  # averaging function


        #######################################################################################################
        # here we need to check the breaks between the last point of contour and the first point of new piece #
        #######################################################################################################
        if (len(final_contour) > 0 and len(avg_cont) > 0):
            last_point = final_contour[-1]
            round_p = avg_cont[0]
            if (max(abs(last_point[0] - round_p[0]), abs(last_point[1] - round_p[1])) > 1):
                # print('####NO_POINT_!!!!!!')
                # here, u have to connect the points
                intermediate_points = get_intermediate(last_point, round_p)
                # print('intermediate_points=', intermediate_points)
                avg_cont = intermediate_points + avg_cont
            #########################################################################

        #######################################################################################################
        final_contour = final_contour + avg_cont
        idx_cc += 1
    #######################################################
    # check if the first and the last point are connected #
    #######################################################
    if len(final_contour) > 0:
        last_point = final_contour[-1]
        round_p = final_contour[0]
        if (max(abs(last_point[0] - round_p[0]), abs(last_point[1] - round_p[1])) > 1):
            intermediate_points = get_intermediate(last_point, round_p)
            final_contour = final_contour + intermediate_points
            ###########################################################
    saf = fill_and_save_contour(final_contour, size_img[0], size_img[1])
    return saf


def get_average_piece_3coord_MEDIAN(connected_component1: List[IdxIj], connected_component2: List[IdxIj],
                                    list_of_all_contours: List[Contour], last_one: bool, all_geodesic_ends, size_img,
                                    d_lim, debug_mode, input_annotations) -> Contour:

    mutual_points = get_mutual_points4new(connected_component1, connected_component2, last_one)
    # mutual points are returned as [a1, a2 - b1, b2]

    if len(mutual_points) == 0:
        print('NO MUTUAL POINTS!')
        return []

    if debug_mode:
        print('average MED cont started')

    flag_for_direction = last_one
    img_sample = np.zeros(size_img)

    point2fit1 = choose_one_point_from_cc(connected_component1, all_geodesic_ends)
    point2fit2 = choose_one_point_from_cc(connected_component2, all_geodesic_ends)

    if point2fit1==point2fit2:
        return [point2fit1]

    if debug_mode:
        print('SELECTED POINTS:', point2fit1, point2fit2)

    ####################################################################################################
    flag_annotation = np.zeros(len(all_geodesic_ends))
    mutual_points_without_dub = []

    for i_points in range(0, len(mutual_points)):
        an_num = mutual_points[i_points][0][0]
        if (flag_annotation[an_num] == 0):
            flag_annotation[an_num] = 1
            mutual_points_without_dub.append(mutual_points[i_points])

    if debug_mode:
        print('mutual_points_without_dub:', mutual_points_without_dub)
    mutual_points = mutual_points_without_dub
    ##############################################################
    # exclude here the piece of contour that has a GE in between the CC
    #############################################################
    all_pieces_to_average = []
    sums_lenth_pieces = []
    skipped = []
    vis_raw = []
    for i_points in range(0, len(mutual_points)):
        # here we have to average
        i1, j1 = mutual_points[i_points][0]  # these are the two points in the same annotation from 2 different cc
        i2, j2 = mutual_points[i_points][1]
        p1 = all_geodesic_ends[i1][j1]
        p2 = all_geodesic_ends[i2][j2]
        cont_3rd_1 = all_geodesic_ends[i1][j1][2]
        cont_3rd_2 = all_geodesic_ends[i2][j2][2]

        if flag_for_direction:
            if cont_3rd_1 < cont_3rd_2:
                p1, p2 = p2, p1
        else:
            if cont_3rd_1 > cont_3rd_2:
                p1, p2 = p2, p1

        if debug_mode:
            print('GEs= from ', p1, ' to ', p2)
            print('a number ', i1, i2, 'should be equal')
        # ok here we need to collect all the contour pieces
        do_we_add, piece_of_contour = get_piece_of_contour4averaging_3coord(p1, p2, list_of_all_contours[i1], i1,
                                                                            all_geodesic_ends, debug_mode)
        if debug_mode:
            print('do_we_add? ', do_we_add)
        piece_con = visualize_contour(img_sample, piece_of_contour)
        vis_raw.append(piece_con)
        #########################################################################
        #
        #########################################################################
        # all_pieces_to_average.append(piece_of_contour)
        if do_we_add:
            all_pieces_to_average.append(piece_of_contour)
            sums_lenth_pieces.append(len(piece_of_contour))
        elif len(all_pieces_to_average) == 0 and i_points == (
                len(mutual_points) - 1):  # we have to add something at least
            all_pieces_to_average.append(piece_of_contour)
            sums_lenth_pieces.append(len(piece_of_contour))
        else:
            skipped.append(i_points)
    ##################################################################
    # ### we will go through it again and will add a piece if the length is not too far from the added
    # # OPTIONAL
    # for i_points in skipped:
    #     # here we have to average
    #     i1, j1 = mutual_points[i_points][0]  # these are the two points in the same annotation from 2 different cc
    #     i2, j2 = mutual_points[i_points][1]
    #     p1 = all_geodesic_ends[i1][j1]
    #     p2 = all_geodesic_ends[i2][j2]
    #     if flag_for_direction:
    #         if cont_3rd_1 < cont_3rd_2:
    #             p1, p2 = p2, p1
    #     else:
    #         if cont_3rd_1 > cont_3rd_2:
    #             p1, p2 = p2, p1
    #     # ok here we need to collect all the contour pieces
    #     # piece_of_contour = get_piece_of_contour(p1, p2, list_of_all_contours[i1])
    #     do_we_add, piece_of_contour = get_piece_of_contour4averaging_3coord(p1, p2, list_of_all_contours[i1], i1,
    #                                                                         all_geodesic_ends, debug_mode)
    #     if (abs(len(piece_of_contour) - sum(sums_lenth_pieces) / len(
    #             sums_lenth_pieces)) < d_lim * 2):  # THIS IT a parameter that depends on the average size
    #         all_pieces_to_average.append(piece_of_contour)
    ####################################################################
    if debug_mode:
        ishow(vis_raw)

    length_list = []
    im_list = []
    for i in range(0, len(all_pieces_to_average)):
        length_list.append(len(all_pieces_to_average[i]))
        im = visualize_contour(img_sample, all_pieces_to_average[i])
        im_list.append(im)
    # this is for visualisation
    if debug_mode:
        show_array_of_images(im_list)

    length_array = np.array(length_list)

    length_list = []
    im_list = []
    distances = []
    # here calculate the length depending on if the point is on the line (+0) on the outter side (+1) or inner side (-1)
    # take the equation of the line and put there every coordinate...maybe its better to take

    a, b, c = compute_plane_2d(point2fit1, point2fit2)
    if debug_mode:
        print('ABC = ', a, b, c)
    for i in range(0, len(all_pieces_to_average)):
        length_list.append((len(all_pieces_to_average[i]), i))
        im = visualize_contour(img_sample,
                               all_pieces_to_average[i])  # this is a BUG FIX , put here an image sample
        im_list.append(im)
        accu = 0
        for point in all_pieces_to_average[i]:
            d = distance2plane(point, a, b, c)
            accu += d
        distances.append((accu, i))
    # this is for visualisation
    if debug_mode:
        show_array_of_images(im_list)  ###########VISUAL

    distances.sort(key=lambda x: x[0])
    if debug_mode:
        print('MED distances', distances)
    median_val = distances[len(distances) // 2]
    if debug_mode:
        print('chosen i= ', len(distances) // 2, 'val = ', median_val)
    median_idx = median_val[1]

    closest_cont = all_pieces_to_average[median_idx]
    closest_cont_tmp, fitted_matrix = fit_contour2two_points_spline_scale1st(closest_cont, point2fit1, point2fit2)

    im123_tmp = visualize_contour_ascending_5(input_annotations[0], closest_cont_tmp)
    im123_tmp[point2fit1[0], point2fit1[1]] = 10
    im123_tmp[point2fit2[0], point2fit2[1]] = 10


    im123 = visualize_contour_ascending_5(input_annotations[0], closest_cont)
    im123[point2fit1[0], point2fit1[1]] = 10
    im123[point2fit2[0], point2fit2[1]] = 10
    im123_fitted_matrix = visualize_contour_ascending_5(input_annotations[0], fitted_matrix)
    im123_fitted_matrix[point2fit1[0], point2fit1[1]] = 10
    im123_fitted_matrix[point2fit2[0], point2fit2[1]] = 10

    if debug_mode:
        ishow([im123, im123_tmp, im123_fitted_matrix])
    # this is the averages piece of contour between the 2 CCs
    return fitted_matrix


def choose_one_point_from_cc(connected_component, all_geodesic_ends):
    # compute the centroid point
    # choose the closest point from the cc
    n = len(connected_component)
    centroid_x = 0
    centroid_y = 0

    points = []

    for p in connected_component:
        i1, j1 = p[0]
        point = all_geodesic_ends[i1][j1]
        points.append(point)
        centroid_x += point[0]
        centroid_y += point[1]
    centroid_x = int(round(centroid_x / n, 0))
    centroid_y = int(round(centroid_y / n, 0))
    centroid_out = (centroid_x, centroid_y)
    return centroid_out



def compute_plane_2d(point1, point2):
    x1, y1 = point1
    x2, y2 = point2
    a = y1 - y2
    b = x2 - x1
    c = x1*y2 - x2*y1
    return a, b, c

def distance2plane(point, a, b, c):
    if a==0 and b==0:
        return 0
    x = point[0]
    y = point[1]
    d = (a*x+b*y+c)/math.sqrt(a**2+b**2)
    return d

def visualize_contour_ascending_5(image: Image, contour: Contour) -> Image:
    visualization_image = np.zeros(image.shape)
    val = 5
    w,h = image.shape
    for point in contour:
        if w>point[0] and h>point[1] and point[0]>0 and point[1]>0:
            visualization_image[point[0], point[1]] = val
        # val = 5
    return visualization_image


# closest_cont = choose_median_length
def fit_contour2two_points_spline_scale1st(contour, point1, point2):
    # shift the contour to the point
    # rotate it, and then scale it
    if (point1 == point2):
        return [], contour

    x_a = contour[0][0]
    y_a = contour[0][1]
    x_a_new = point1[0]
    y_a_new = point1[1]

    x_b = contour[len(contour) - 1][0]
    y_b = contour[len(contour) - 1][1]
    x_b_new = point2[0]
    y_b_new = point2[1]

    d_before = math.sqrt((x_a - x_b) ** 2 + (y_a - y_b) ** 2)
    d_after = math.sqrt((x_a_new - x_b_new) ** 2 + (y_a_new - y_b_new) ** 2)

    if d_before == 0:
        scale = 1
    else:
        scale = d_after / d_before

    # we shift to point A
    shifted2a = [(point[0] + (x_a_new - x_a), point[1] + (y_a_new - y_a)) for point in contour]

    x_b_shifted = shifted2a[-1][0]
    y_b_shifted = shifted2a[-1][1]

    v_a_newend = [x_b_shifted - x_a_new, y_b_shifted - y_a_new]
    v_a_b = [x_b_new - x_a_new, y_b_new - y_a_new]
    cross_product = np.cross(v_a_newend, v_a_b)

    angle4rotation = abs(math.atan2(v_a_newend[1], v_a_newend[0]) - math.atan2(v_a_b[1], v_a_b[0]))
    if math.degrees(angle4rotation) > 90:
        angle4rotation = abs(2 * 3.1415 - angle4rotation)

    if cross_product < 0:
        angle4rotation *= -1.0

    cos = np.cos(angle4rotation)
    sin = np.sin(angle4rotation)

    coordinates_x = [x for x, y in shifted2a]
    coordinates_y = [y for x, y in shifted2a]

    points = np.array([coordinates_x, coordinates_y]).T
    distance = np.cumsum(np.sqrt(np.sum(np.diff(points, axis=0) ** 2, axis=1)))
    distance = np.insert(distance, 0, 0) / distance[-1]

    alpha = np.linspace(0, 1, len(contour) * 3)

    interpolator = interp1d(distance, points, kind='slinear', axis=0)
    interpolated_points = interpolator(alpha)

    interpolated_list = [(x, y) for x, y in zip(interpolated_points[:, 0], interpolated_points[:, 1])]

    transformed_c = [linear_transfom_sin_cos(point, point1, sin, cos, scale) for point in interpolated_list]

    transformed_c_inter_wt = list(dict.fromkeys(transformed_c))
    return (shifted2a, transformed_c_inter_wt)



def linear_transfom_sin_cos(point,point00, alpha_sin, alpha_cos, scale):
    x, y = point
    x0, y0 = point00
    x_t = (alpha_cos * (x-x0) - alpha_sin * (y-y0))
    y_t = (alpha_sin * (x-x0) + alpha_cos * (y-y0))
    x_t,y_t = int(round(x_t*scale+x0, 0)), int(round(y_t*scale+y0, 0))
    return (x_t,y_t)
