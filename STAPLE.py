
import numpy as np
from skimage import io
import skimage.viewer
import SimpleITK as sitk
from visualization import ishow

def STAPLEFromSITK(segmentations, threshold):
    foregroundValue = 1
    reference_segmentation_STAPLE_probabilities = sitk.STAPLE(segmentations, foregroundValue)
    # We use the overloaded operator to perform thresholding, another option is to use the BinaryThreshold function.
    reference_segmentation_STAPLE = reference_segmentation_STAPLE_probabilities > threshold
    arr = sitk.GetArrayFromImage(reference_segmentation_STAPLE)
    return arr


def apply_STAPLE_sitk(array_img):
    folder_global = 'tmp/'
    number_of_images = len(array_img)
    for i, cont in enumerate(array_img):
        cont[cont > 0] = 1
        cont = np.uint8(cont)
        skimage.io.imsave(fname=folder_global + str(i) + '_sitk.tif', arr=cont, check_contrast=False)

    testForSTAPLEimgs = [sitk.ReadImage((folder_global + str(name) + '_sitk.tif'), sitk.sitkUInt8) for name in
                         range(number_of_images)]

    MASK_staple = STAPLEFromSITK(testForSTAPLEimgs, 0.8)
    MASK_staple[MASK_staple > 0] = 1
    return MASK_staple