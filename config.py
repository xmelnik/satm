#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# import torch
import numpy as np

import torch

config = {

    # General parameters
    "dim": 2,                       # Image dimension (2 or 3)
    "use_gpu": False,               # Fit the algorithm on GPU (usually not necessary)
    "torch_dtype": torch.double,    # Type for torch tensors (needs to be in line with the type of the numpy arrays)
    "numpy_dtype": np.float64,      # Type for the numpy arrays (needs to be in line with the type of the torch tensors)
    "tol": 1e-5,                    # Threshold on the lower bound to stop the algorithm
    "n_iter": 100,                  # Maximal number of iterations
    "eps": 1e-8,                    # Epsilon

    # Parameters specif to the Student model
    "nr_tol": 1e-6,                 # Threshold when updating the degrees of freedom
    "nr_n_iter": 1000,              # Maximal number of iterations when updating the degrees of freedom

    # Input type and link function
    "link_function": "logit",        # Can be either the square root function 'sqrt' or the logit function 'logit'
    "distance": "euclidean",        # Can be either 'euclidean' if the raters' masks are binary and need to be
                                    # transformed into probabilities using an Euclidean distance transform, or
                                    # 'precomputed' if the raters' masks are already probabilities
    "lambda_expit": 1,              # Coefficient of the sigmoid to convert the Euclidean distance map into probabilities

    # Cropping parameters
    # If the input image is large but the segmented structure is small, we can crop the inputs around
    # the segmentation to save computation time
    "crop": False,                  # True if we want to crop the image, False otherwise
    "crop_margin": 10,              # Extra amount of voxels to keep around the segmentation (at least 3)

    # Narrow band parameters
    # If the segmentation is large, it can take time to fuse the raters masks. However, the interesting regions are the
    # areas where the raters disagree. As an approximation in order to save some time, we can choose to fit the
    # algorithm on a narrow band defined as the regions where the raters disagree plus some extra voxels. Outside of
    # the narrow band, the consensus will be taken as a mean among the raters.
    "use_narrow_band": True,       # True if we want to use the approximation, False otherwise
    "nb_margin": 10,                # Amount of extra voxels for each side of the narrow band (at least 5)

    # Model we want to use
    "model": "Gaussian",            # Model we want to use: 'Gaussian' (non robust approach) or 'Student'
                                    # (robust approach)

    # Parameters for the spatial regularization
    # Key parameters are the step and scale of the basis functions. The larger they are, the smoother is the output.
    # They should be chosen such that we have overlapping basis, i.e. scale > step. Moreover, a particular attention
    #  needs to be paid if we use the narrow band. In such case, the step needs to be smaller than the narrow band
    #  margin, in order insure the presence of basis on the narrow band
    "spatial_prior": False,          # True if we want to regularize the ouput, False otherwise
    "layout": "square",             # Layout of the basis functions in the image. Can be 'square' or 'quinconce'
    "step": 5,                     # Step between each basis functions
    "scale": 20                     # Scale of the basis functions

}
